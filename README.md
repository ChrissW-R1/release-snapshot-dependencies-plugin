# Release Snapshot Dependencies Maven Plugin

This Maven plugin handles dependencies, which will never released by their
developers. This is very sad and breaks the versioning rules of Maven and clean
programming.

So it is a bad practice to use this plugin!
